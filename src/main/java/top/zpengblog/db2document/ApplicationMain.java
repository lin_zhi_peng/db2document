package top.zpengblog.db2document;

import top.zpengblog.db2document.ui.DB2DocumentMenuUI;

/**
 * @author ZhiPeng.Lin (codingjava@qq.com)
 * @version 1.0.0
 * @date 2022/1/29 0:41
 */
public class ApplicationMain {
    public static void main(String[] args) {
        new DB2DocumentMenuUI();
    }
}
