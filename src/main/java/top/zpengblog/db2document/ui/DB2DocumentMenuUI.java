package top.zpengblog.db2document.ui;

import top.zpengblog.db2document.constant.SysConstant;
import top.zpengblog.db2document.ui.listenner.ChooseTableListener;
import top.zpengblog.db2document.ui.listenner.TestConnectionListener;

import javax.swing.*;
import java.awt.*;

/**
 * 主界面，维护数据库连接信息.
 *
 * @author ZhiPeng.Lin (codingjava@qq.com)
 * @version 1.0.0
 * @date 2022/1/27 23:48
 */
public class DB2DocumentMenuUI extends JFrame{

    private JTextField address = new JTextField();
    private JTextField port = new JTextField();
    private JTextField database = new JTextField();
    private JTextField userName = new JTextField();
    private JTextField userPassword = new JTextField();

    /**
     * 初始化界面.
     */
    public DB2DocumentMenuUI() {

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 600, 430);
        getContentPane().add(panel);
        panel.setLayout(null);

        // 中间面板
        JPanel middlePanel = new JPanel();
        middlePanel.setBounds(8, 15, 579, 360);
        middlePanel.setBackground(Color.WHITE);
        middlePanel.setLayout(null);
        panel.add(middlePanel);

        initTextField(middlePanel);

        // 按钮面板
        JPanel btnPanel = new JPanel();
        btnPanel.setBounds(8, 385, 579, 35);
        btnPanel.setLayout(null);

        JButton chooseTableBtn = new JButton(SysConstant.CHOOSE_TABLE_BTN);
        chooseTableBtn.setBounds(453, 5, 125, 25);
        chooseTableBtn.setFont(SysConstant.DEFAULT_FONT);
        chooseTableBtn.setBackground(SysConstant.DEFAULT_BTN_COLOR);
        chooseTableBtn.addActionListener(new ChooseTableListener(address, port, database, userName, userPassword));
        // 测试连接成功才能选择
        chooseTableBtn.setEnabled(false);
        JButton testConnectionBtn = new JButton(SysConstant.TEST_CONNECT_BTN);
        testConnectionBtn.setBackground(SysConstant.DEFAULT_BTN_COLOR);
        testConnectionBtn.setFont(SysConstant.DEFAULT_FONT);
        testConnectionBtn.setBounds(0, 5, 125, 25);
        testConnectionBtn.addActionListener(new TestConnectionListener(address, port, database, userName, userPassword, chooseTableBtn));
        btnPanel.add(testConnectionBtn);
        btnPanel.add(chooseTableBtn);
        panel.add(btnPanel);

        // 设置窗口大小，屏幕居中
        setTitle(SysConstant.SYSTEM_NAME);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        getContentPane().add(panel);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int screenWidth = (int) screenSize.getWidth();
        int screenHeight = (int) screenSize.getHeight();
        int x = screenWidth / 2 - 300;
        int y = screenHeight / 2 - 229;
        setBounds(x, y, 600, 459);
        this.setResizable(false);
        setVisible(true);
    }

    private void initTextField(JPanel panel) {
        JLabel dbType = new JLabel("数据库类型 : ");
        dbType.setBounds(SysConstant.LABEL_LEFT, 15, 150, 50);
        dbType.setFont(SysConstant.LABEL_FONT);

        JLabel addressLabel = new JLabel("主机 : ");
        addressLabel.setBounds(SysConstant.LABEL_LEFT, 65, 100, 50);
        addressLabel.setFont(SysConstant.LABEL_FONT);

        JLabel portLabel = new JLabel("端口 : ");
        portLabel.setBounds(SysConstant.LABEL_LEFT, 115, 100, 50);
        portLabel.setFont(SysConstant.LABEL_FONT);

        JLabel dbNameLabel = new JLabel("数据库名称 : ");
        dbNameLabel.setBounds(SysConstant.LABEL_LEFT, 165, 150, 50);
        dbNameLabel.setFont(SysConstant.LABEL_FONT);

        JLabel nameLabel = new JLabel("用户名 : ");
        nameLabel.setBounds(SysConstant.LABEL_LEFT, 215, 150, 50);
        nameLabel.setFont(SysConstant.LABEL_FONT);
        JLabel passwordLabel = new JLabel("密码 : ");
        passwordLabel.setBounds(SysConstant.LABEL_LEFT, 265, 100, 50);
        passwordLabel.setFont(SysConstant.LABEL_FONT);

        panel.add(addressLabel);
        panel.add(passwordLabel);
        panel.add(nameLabel);
        panel.add(dbNameLabel);
        panel.add(portLabel);
        panel.add(dbType);

        JTextField dbTypeField = new JTextField();
        dbTypeField.setBounds(130, 25, 160, 30);
        dbTypeField.setText(SysConstant.DEFAULT_DB_TYPE);
        dbTypeField.setEditable(false);
        dbTypeField.setToolTipText("暂时只支持MYSQL类型");

        address.setBounds(130, 75, 300, 30);
        address.setText("127.0.0.1");
        address.setToolTipText("参考格式：127.0.0.1");

        port.setBounds(130, 125, 60, 30);
        port.setText("3306");
        port.setToolTipText("参考格式：3306");

        database.setBounds(130, 175, 300, 30);
        database.setText("");
        database.setToolTipText("输入要映射的数据库名称");

        userName.setBounds(130, 225, 160, 30);
        userName.setText("root");
        userName.setToolTipText("输入用户名");

        userPassword.setBounds(130, 275, 160, 30);
        userPassword.setToolTipText("输入密码");

        panel.add(address);
        panel.add(port);
        panel.add(database);
        panel.add(userName);
        panel.add(userPassword);
        panel.add(dbTypeField);
    }
}
