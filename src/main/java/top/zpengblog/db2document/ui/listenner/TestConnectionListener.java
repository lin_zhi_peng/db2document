package top.zpengblog.db2document.ui.listenner;

import top.zpengblog.db2document.jdbc.ConnectionManager;
import top.zpengblog.db2document.model.ConnectionInfo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

/**
 * @author ZhiPeng.Lin (codingjava@qq.com)
 * @version 1.0.0
 * @date 2022/1/27 23:48
 */
public class TestConnectionListener implements ActionListener {

    private ConnectionInfo connectionInfo;
    private JButton chooseTableBtn;

    @Override
    public void actionPerformed(ActionEvent e) {

        Connection connection = ConnectionManager.getConnectionByCache(connectionInfo);
        if (null != connection) {
            JOptionPane.showMessageDialog(null, "连接成功！", "结果", JOptionPane.PLAIN_MESSAGE);
            chooseTableBtn.setEnabled(true);
        }
    }

    public TestConnectionListener(JTextField address, JTextField port, JTextField database,
                                  JTextField userName, JTextField userPassword, JButton chooseTableBtn) {
        this.connectionInfo = new ConnectionInfo(address, port, database, userName, userPassword);
        this.chooseTableBtn = chooseTableBtn;
    }

    public TestConnectionListener() {
    }
}
