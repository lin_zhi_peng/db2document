package top.zpengblog.db2document.constant;

/**
 * @author ZhiPeng.Lin (codingjava@qq.com)
 * @version 1.0.0
 * @date 2022/1/29 14:45
 */
public class PrimaryKeyConstant {
    public static final String TABLE_CAT ="TABLE_CAT";
    public static final String TABLE_NAME ="TABLE_NAME";
    public static final String PK_NAME ="PK_NAME";
    public static final String COLUMN_NAME ="COLUMN_NAME";
}
