package top.zpengblog.db2document.constant;

import java.awt.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 系统常量.
 *
 * @author ZhiPeng.Lin (codingjava@qq.com)
 * @version 1.0.0
 * @date 2022/1/27 23:48
 */
public class SysConstant {
    public static final String SYSTEM_NAME = "数据库设计文档生成工具   —— by codingjava@qq.com";
    public static final String TEST_CONNECT_BTN = "测试连接";
    public static final String CHOOSE_TABLE_BTN = "生成设计文档";
    public static final int LABEL_LEFT = 25;
    public static final Font DEFAULT_FONT = new Font("宋体", Font.PLAIN, 12);
    public static final Font LABEL_FONT = new Font("黑体", Font.PLAIN, 16);
    public static final Color DEFAULT_BTN_COLOR = new Color(225, 225, 225);
    public static final String DEFAULT_DB_TYPE = "MYSQL";
    public static final String MYSQL_DRIVER_CLASS = "com.mysql.jdbc.Driver";
    public static final String MYSQL_URL_TEMPLATE = "jdbc:mysql://%s:%s/%s?useUnicode=true&characterEncoding=UTF-8&useSSL=false";
    public static final String DEFAULT_FILE_NAME = "数据库设计文档";
    public static final String Y = "Y";
    public static final String N = "N";
    public static final String ZERO = "0";
    public static AtomicInteger VERSION_NUMBERS = new AtomicInteger(1);
}
