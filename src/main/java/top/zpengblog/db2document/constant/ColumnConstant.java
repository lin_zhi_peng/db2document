package top.zpengblog.db2document.constant;

/**
 * @author ZhiPeng.Lin (codingjava@qq.com)
 * @version 1.0.0
 * @date 2022/1/28 21:55
 */
public class ColumnConstant {
    public static final String TABLE_NAME = "TABLE_NAME";
    public static final String COLUMN_NAME = "COLUMN_NAME";
    public static final String DATA_TYPE = "DATA_TYPE";
    public static final String COLUMN_SIZE = "COLUMN_SIZE";
    public static final String REMARKS = "REMARKS";
    public static final String DECIMAL_DIGITS = "DECIMAL_DIGITS";
    public static final String IS_NULLABLE = "IS_NULLABLE";
    public static final String NULLABLE = "NULLABLE";
    public static final String COLUMN_DEF = "COLUMN_DEF";
    public static final String ORDINAL_POSITION = "ORDINAL_POSITION";
    public static final String TYPE_NAME = "TYPE_NAME";
    public static final String COLUMN_TYPE = "COLUMN_TYPE";
    public static final String COLUMN_LENGTH = "COLUMN_LENGTH";
}
