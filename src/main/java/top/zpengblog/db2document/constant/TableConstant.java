package top.zpengblog.db2document.constant;

/**
 * @author ZhiPeng.Lin (codingjava@qq.com)
 * @version 1.0.0
 * @date 2022/1/28 21:54
 */
public class TableConstant {
    public static final String TABLE_NAME = "TABLE_NAME";
    public static final String TABLE_REMARK = "REMARKS";
    public static final String TABLE_CAT = "TABLE_CAT";
}
