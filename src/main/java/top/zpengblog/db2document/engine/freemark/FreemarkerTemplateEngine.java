/*
 * screw-core - 简洁好用的数据库表结构文档生成工具
 * Copyright © 2020 SanLi (qinggang.zuo@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package top.zpengblog.db2document.engine.freemark;

import cn.hutool.core.lang.Assert;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.lang3.StringUtils;
import top.zpengblog.db2document.engine.AbstractTemplateEngine;
import top.zpengblog.db2document.engine.EngineConfig;
import top.zpengblog.db2document.engine.EngineTemplateType;
import top.zpengblog.db2document.model.DataModel;
import top.zpengblog.db2document.util.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Locale;
import java.util.Objects;

/**
 * freemarker template produce
 *
 * @author SanLi
 * Created by qinggang.zuo@gmail.com / 2689170096@qq.com on 2020/3/17 21:40
 */
public class FreemarkerTemplateEngine extends AbstractTemplateEngine {
    /**
     * freemarker 配置实例化
     */
    private final Configuration configuration = new Configuration(
        Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);

    {
        try {
            String path = getEngineConfig().getCustomTemplate();
            //自定义模板
            if (StringUtils.isNotBlank(path) && FileUtils.isFileExists(path)) {
                //获取父目录
                String parent = Objects.requireNonNull(FileUtils.getFileByPath(path)).getParent();
                //设置模板加载路径
                configuration.setDirectoryForTemplateLoading(new File(parent));
            }
            //加载自带模板
            else {
                //模板存放路径
                configuration.setTemplateLoader(
                    new ClassTemplateLoader(this.getClass(), EngineTemplateType.freemarker.getTemplateDir()));
            }
            //编码
            configuration.setDefaultEncoding("UTF-8");
            //国际化
            configuration.setLocale(new Locale("zh_CN"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public FreemarkerTemplateEngine(EngineConfig templateConfig) {
        super(templateConfig);
    }

    /**
     * 生成文档
     *
     * @param info {@link DataModel}
     */
    @Override
    public void produce(DataModel info, String docName) throws Exception {
        Assert.notNull(info, "DataModel can not be empty!");
        String path = getEngineConfig().getCustomTemplate();
        try {
            Template template;
            // freemarker template
            // 如果自定义路径不为空文件也存在
            if (StringUtils.isNotBlank(path) && FileUtils.isFileExists(path)) {
                // 文件名称
                String fileName = new File(path).getName();
                template = configuration.getTemplate(fileName);
            }
            //获取系统默认的模板
            else {
                template = configuration
                    .getTemplate(getEngineConfig().getFileType().getTemplateNamePrefix()
                                 + EngineTemplateType.freemarker.getSuffix());
            }
            // create file
            File file = getFile(docName);
            // writer freemarker
            try (Writer out = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
                // process
                template.process(info, out);
                // open the output directory
                openOutputDir();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
