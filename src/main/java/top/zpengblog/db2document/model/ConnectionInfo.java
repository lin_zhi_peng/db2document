package top.zpengblog.db2document.model;

import javax.swing.*;

/**
 * 数据库连接信息.
 *
 * @author ZhiPeng.Lin (codingjava@qq.com)
 * @version 1.0.0
 * @date 2022/1/27 23:48
 */
public class ConnectionInfo {

    private JTextField address;
    private JTextField port;
    private JTextField database;
    private JTextField userName;
    private JTextField userPassword;

    public ConnectionInfo() {
    }

    public ConnectionInfo(JTextField address, JTextField port, JTextField database, JTextField userName, JTextField userPassword) {
        this.address = address;
        this.port = port;
        this.database = database;
        this.userName = userName;
        this.userPassword = userPassword;
    }

    public String getAddress() {
        return address.getText();
    }

    public void setAddress(JTextField address) {
        this.address = address;
    }

    public String getPort() {
        return port.getText();
    }

    public void setPort(JTextField port) {
        this.port = port;
    }

    public String getDatabase() {
        return database.getText();
    }

    public void setDatabase(JTextField database) {
        this.database = database;
    }

    public String getUserName() {
        return userName.getText();
    }

    public void setUserName(JTextField userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword.getText();
    }

    public void setUserPassword(JTextField userPassword) {
        this.userPassword = userPassword;
    }

    @Override
    public String toString() {
        return "ConnectionInfo{" +
                "address='" + address.getText() + '\'' +
                ", port='" + port.getText() + '\'' +
                ", database='" + database.getText() + '\'' +
                ", userName='" + userName.getText() + '\'' +
                ", userPassword='" + userPassword.getText() + '\'' +
                '}';
    }
}
