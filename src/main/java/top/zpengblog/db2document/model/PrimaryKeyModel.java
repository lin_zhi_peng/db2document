package top.zpengblog.db2document.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ZhiPeng.Lin (codingjava@qq.com)
 * @version 1.0.0
 * @date 2022/1/29 14:57
 */
@Data
public class PrimaryKeyModel implements Serializable {
    private static final long serialVersionUID = 2680610656024956643L;
    private String tableCat;
    private String tableName;
    private String pkName;
    private String columnName;
}
