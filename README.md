# 数据库设计文档生成工具

## 介绍
**db2document** 是基于 **Java GUI** 开发的一款简单好用的**数据库设计文档生成工具**，支持通过界面输入数据源连接信息，然后自动输出对应的数据库设计文档。为了更方便使用，最后把jar打包成了 exe 文件，目前输出文档格式支持word文档，文档输出这块儿参考 **screw** 开源项目的模板引擎编写。

## 支持数据源

- [x] MySQL
- [ ] Oracle （待开发）
- [ ] Sql Server （待开发）
- [ ] TiDB （待开发）
- [ ] 达梦 （待开发）
- [ ] 更多...

## 功能特性

- 代码简洁、使用简单
- 界面输入支持，可灵活安装

## 项目结构

``` lua
db2document
├── constant -- 系统常量
├── engine -- 文档输出模板引擎
    ├── freemark -- 具体模板引擎
├── jdbc -- 数据源元数据采集
├── model -- 元数据实体
├── ui -- GUI 界面
    ├── listenner -- 按钮监听器
├── util -- 工具类
```

## 程序演示

![image-20220204152602129](https://img-blog.csdnimg.cn/31ca54b28dfe4a2f8e01fdc8bb767816.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5p6X5b-X6bmPSkFWQQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

![image-20220204152602129](https://img-blog.csdnimg.cn/ffbc58384f9f4d53b20966e523195389.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5p6X5b-X6bmPSkFWQQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

## 文档效果
> word

![image-20220204152602129](https://img-blog.csdnimg.cn/118af6bba7bb475ea09ebd4b0bae8b83.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5p6X5b-X6bmPSkFWQQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

## 说明
只是为了方便平时的数据库设计文档生成写的一个小工具，其他开源的框架都很好，本项目也是参考了其他开源项目很棒的设计思路。
由于其他的项目大多改源码来生成，使用还是不太方便，于是本人想通过界面的形式，简化操作的过程。
为了更加傻瓜式化，本人把工程jar包直接打成了 exe 程序(exeutil目录提供了打包工具)，安装完毕下次直接使用即可，不必使用 Java 命令启动和安装 Java 运行环境

欢迎访问个人站点：https://www.zpengblog.top
